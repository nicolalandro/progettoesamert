import pandas as pd
from bokeh.io import curdoc
from bokeh.models import Step, ColumnDataSource
from bokeh.palettes import viridis
from bokeh.plotting import figure, output_file, show

from ordered_set import OrderedSet

TOOLS = "pan,wheel_zoom,box_zoom,reset,save"
df = pd.read_csv("first_full.csv")

keys = df.keys()

labels = OrderedSet()
times = []
values = []

for k in keys[2:]:
    values.append([])
    for index, row in df.iterrows():
        labels.add(k + str(row[k]))

for index, row in df.iterrows():
    times.append(row[keys[0]])
    for i, k in enumerate(keys[2:]):
        values[i].append(k + str(row[k]))

output_file("uvision.html")
curdoc().theme = 'dark_minimal'
mypalette = viridis(len(values)+4)[4:]
plot = figure(y_range=list(labels), plot_width=5000, plot_height=580, tools=TOOLS)
for i, v in enumerate(values):
    source = ColumnDataSource(data={'x': times, 'y': v})
    glyph = Step(x='x', y='y', line_width=2, line_color=mypalette[i], mode="center")
    plot.add_glyph(source, glyph)

curdoc().add_root(plot)
