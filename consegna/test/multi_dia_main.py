import os
import pandas as pd
from bokeh.io import curdoc
from bokeh.layouts import gridplot, layout
from bokeh.models import Step, ColumnDataSource, CustomJS
from bokeh.palettes import viridis
from bokeh.plotting import figure, output_file, show
from bokeh.models.formatters import NumeralTickFormatter
from bokeh.layouts import widgetbox
from bokeh.models.widgets import CheckboxButtonGroup

from ordered_set import OrderedSet

TOOLS = ['pan', 'xwheel_zoom', 'box_zoom', 'reset', 'previewsave', 'hover']
df = pd.read_csv(os.getenv('file'))

keys = df.keys()

labels = OrderedSet()
times = []
values = []

for k in keys[2:]:
    values.append([])

for index, row in df.iterrows():
    times.append(row[keys[0]])
    for i, k in enumerate(keys[2:]):
        values[i].append(row[k])

curdoc().theme = 'dark_minimal'
#mypalette = viridis(len(values) + 4)[4:]
mypalette = viridis(4)[1:]

fig = []
first_figure = None
checkbox_labels = []

for i, v in enumerate(values):
	if i == 0:
		plot = figure(
			title=keys[2:][i],
			plot_width=1070,
			plot_height=250,
			tools=TOOLS,
			tooltips=[("(x,y)", "($x, $y)")]
		)
		first_figure = plot
	else:
		plot = figure(
			title=keys[2:][i],
			plot_width=1070,
			plot_height=250,
			tools=TOOLS,
			x_range = first_figure.x_range,
			tooltips=[("(x,y)", "($x, $y)")]
		)	
	checkbox_labels.append(keys[2:][i])
	plot.xaxis.formatter = NumeralTickFormatter(format='0.000')
	source = ColumnDataSource(data={'x': times, 'y': v})
	glyph = Step(x='x', y='y', line_width=2, line_color=mypalette[i % 3], mode="center")
	plot.add_glyph(source, glyph)
	fig.append([plot])

checkbox_button_group = CheckboxButtonGroup(labels=checkbox_labels)

widget = widgetbox(checkbox_button_group)

p = None

def checkboxchange(attr,new,old):
	plots = []
	for aind in checkbox_button_group.active:
		plots.append(fig[aind][0])
	p.children = plots

checkbox_button_group.on_change('active', checkboxchange)

p = gridplot([])

l = layout([[widget, p]], sizing_mode='fixed')

curdoc().add_root(l)
