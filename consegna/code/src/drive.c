#include <RTL.h>             // RTX kernel functions & defines
#include <stm32f10x.h>       // needed for GPIOB
#include "state.h"
#include "wait.h"

extern volatile int IDLE;
int DRIVE = 0;

DriveActionState actual_drive_actions; //for debug, read by InputSimulator

extern DriveActionState drive_action_state;

void do_accelerate() {
	actual_drive_actions.throttle = ACCELERATE;
}

void do_break() {
	actual_drive_actions.throttle = BREAK;
}

void do_not_throttle() {
	actual_drive_actions.throttle = NO_ACTION;
}

void do_steer_left() {
	actual_drive_actions.steer = STEER_LEFT;
}

void do_steer_right() {
	actual_drive_actions.steer = STEER_RIGHT;
}

void do_change_lane_left() {
	actual_drive_actions.steer = CHANGE_LANE_LEFT;
}

void do_change_lane_right() {
	actual_drive_actions.steer = CHANGE_LANE_RIGHT;
}

void do_not_steer() {
	actual_drive_actions.steer = NO_ACTION;
}

void drive ()  {
	switch(drive_action_state.steer) {
		case STEER_LEFT:
			do_steer_left();
			break;
		case STEER_RIGHT:
			do_steer_right();
			break;
		case CHANGE_LANE_LEFT:
			do_change_lane_left();
			break;
		case CHANGE_LANE_RIGHT:
			do_change_lane_right();
			break;
		default: 
			do_not_steer();
	}
	
	switch(drive_action_state.throttle) {
		case ACCELERATE:
			do_accelerate();
			break;
		case BREAK:
			do_break();
			break;
		default:
			do_not_throttle();
	}
}

__task void Drive(void) {
	while(LIVE) {
		IDLE = 0;
		DRIVE = 1;
		
		busy_wait(1);
		drive();
		
		DRIVE = 0;
		
		sleep(9);
	}
	
	do_not_steer();
	do_not_throttle();
	
	os_tsk_delete_self();
}
