#define MAX_PRIORITY 254
#define MIN_PRIORITY 1

extern char LIVE;

//DRIVE -> steering actions
#define NO_ACTION 0
#define STEER_LEFT  1
#define STEER_RIGHT 2
#define CHANGE_LANE_LEFT 3
#define CHANGE_LANE_RIGHT 4

//DRIVE -> throttle actions
#define BREAK  1
#define ACCELERATE  2

//DRIVE -> halt actions
#define GOING_TO_HALT 1
#define ON_HALT 2

typedef struct DriveActionState {
  char steer;
  char throttle;
  long halt_latitude;
  long halt_longitude;
	char halt;
} DriveActionState;

// POSITION
typedef struct PositionState {
	long latitude;
	long longitude;
	int timestamp;
}	PositionState;

// INTERNAL SENSORS
typedef struct VehicleInternalState {
	int oil_level;
	int fuel_level;
}	VehicleInternalState;

// EXTERNAL SENSORS
#define LANE_LEFT 1
#define LANE_RIGHT 2
#define NO_SEM 0
#define SEM_RED 1
#define SEM_GREEN 2
#define DETECTED 1
#define NOT_DETECTED 0

// OTHER CARS COMMUNICATION
typedef struct SlowcarSignal {
	char lane_position;
	int car_id;
} SlowcarSignal;

typedef struct VehicleExternalState {
	// user input
	long destination_lat;
	long destination_lon;
	char stop_requested;
	char restart_requested;
	// sensors
	char lane_position;
	// road sign
	char semaphore;
	// other cars communication
	SlowcarSignal slowcar_signal;
} VehicleExternalState;

// SENSORS
typedef struct PositionSensorsData {
	long latitude;
	long longitude;
	int timestamp;
} PositionSensorsData;

typedef struct Sensors {
	PositionSensorsData position;
	int oil_level;
	int fuel_level;
	char lane_position;
	char stop_requested;
	char restart_requested;
	char semaphore;
	SlowcarSignal slowcar_signal;
	char proximity;
} Sensors;
