#include <RTL.h>             // RTX kernel functions & defines
#include "state.h"
#include "wait.h"

extern volatile int IDLE;
extern Sensors sensors;
extern VehicleInternalState vehicle_internal_state;
int UPDATE_INTERNAL = 0;

__task void UpdateInternalSensors(void) {
	while(LIVE) {
		IDLE = 0;
		UPDATE_INTERNAL = 1;
		vehicle_internal_state.oil_level = sensors.oil_level;
		vehicle_internal_state.fuel_level = sensors.fuel_level;
		UPDATE_INTERNAL = 0;
		sleep(1);
	}
	
	os_tsk_delete_self();
}
