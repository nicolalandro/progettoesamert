#include "state.h"

int DEFAULT_OIL_LEVEL = 1000;
int DEFAULT_FUEL_LEVEL = 1000;

extern int SIMULATOR_EVENT_ID;
extern Sensors sensors;
extern DriveActionState actual_drive_actions;
extern VehicleExternalState vehicle_external_state;

extern char refuel_pending;

void set_defaults(int oil_level, int fuel_level, long lat, long lon) {
	sensors.oil_level = oil_level;
	sensors.fuel_level = fuel_level;
	sensors.lane_position = LANE_RIGHT;
	// User Input
	vehicle_external_state.destination_lat = lat;
	vehicle_external_state.destination_lon = lon;
}

void set_position() {
	sensors.position.latitude += 1;
	sensors.position.longitude += 1;
	sensors.position.timestamp += 1;			
}

void set_fuel_and_oil() {
	sensors.oil_level -= 1;
	sensors.fuel_level -= 1;
}

void update_actual_lane_position() {
	switch(actual_drive_actions.steer) {
		case CHANGE_LANE_LEFT:
			sensors.lane_position = LANE_LEFT;
			break;
		case CHANGE_LANE_RIGHT:
			sensors.lane_position = LANE_RIGHT;
			break;
	}
}

void do_refuel_if_needed() {
	static int rounds = 0;
	if(refuel_pending && rounds++ > 20) {
		rounds = 0;
		refuel_pending = 0;
		sensors.oil_level = DEFAULT_OIL_LEVEL;
		sensors.fuel_level = DEFAULT_FUEL_LEVEL;
	}
}

void set_stop_and_restart_requested(int simulator_timer, int start_time, int button_click_duration, int total_halt_duration) {
	if(simulator_timer < start_time) {
		sensors.stop_requested = 0;
	} else if (simulator_timer > (start_time + button_click_duration) && simulator_timer < (start_time + button_click_duration*2)) {
		sensors.stop_requested = 1;
	} else if (simulator_timer >= (start_time + button_click_duration*2) && simulator_timer < (start_time + button_click_duration*3)){
		sensors.stop_requested = 0;
	} else if (simulator_timer >= (start_time + button_click_duration*3) && simulator_timer < start_time + total_halt_duration) {
		sensors.restart_requested = 1;
	} else {
		sensors.restart_requested = 0;
	}
}

void set_semaphore(int simulator_timer, int start_time, int red_duration, int green_duration) {
	if (simulator_timer >= start_time && simulator_timer < (start_time + red_duration)) {
		sensors.semaphore = SEM_RED;
	} else if (simulator_timer >= (start_time + red_duration) && simulator_timer < (start_time + red_duration + green_duration)) {
		sensors.semaphore = SEM_GREEN;
	} else {
		sensors.semaphore = NO_SEM;
	}
}

void set_slowcar_on_lane(int simulator_timer, int start_time, int signal_duration, char lane_position, int car_id) {
		if(simulator_timer >= start_time && simulator_timer <= start_time + signal_duration) {
			sensors.slowcar_signal.lane_position = lane_position;
			sensors.slowcar_signal.car_id = car_id;
		} else {
			sensors.slowcar_signal.lane_position = NO_ACTION;
			sensors.slowcar_signal.car_id = NO_ACTION;
		}
}

void set_proximity_sensor(int simulator_timer, int start_time, int signal_duration){
		if(simulator_timer >= start_time && simulator_timer <= start_time + signal_duration) {
			sensors.proximity = DETECTED;
		} else {
			sensors.proximity = NOT_DETECTED;
		}
}
