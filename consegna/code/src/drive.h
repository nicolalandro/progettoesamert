#include <RTL.h>             // RTX kernel functions & defines

__task void Drive(void);

void do_accelerate(void);
void do_break(void);
void do_change_lane_left(void);
void do_change_lane_right(void);
void do_not_steer(void);
void do_not_throttle(void);
