#include <RTL.h>             // RTX kernel functions & defines
#include "state.h"
#include "wait.h"
#include "input_simulator_helper.h"

#define CAR_ID 123

#define TEST_USER_REQUESTS_STOP 1000,1000,100,100
#define TEST_USER_REQUESTS_STOP_NEAR_DESTINATION 1000,1000,70,70

#define TEST_ARRIVAL_AT_DESTINATION 1000,1000,50,50
#define TEST_SLOWCAR_LANE_RIGHT 1000,1000,100,100
#define TEST_SEMAPHORE 1000,1000,100,100

#define TEST_LOW_FUEL 1000,210,100,100
#define TEST_LOW_OIL 210,1000,100,100
#define TEST_PROXIMITY_SENSOR 1000,1000,100,100

#define TEST_FULL 1000,1000,230,230

extern volatile int IDLE;
int SIMULATOR = 0;
extern DriveActionState drive_action_state;
int simulator_timer = 0;

void test_user_requests_stop() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
	}
	set_stop_and_restart_requested(simulator_timer, 10, 50, 160);
	do_refuel_if_needed();
}

void test_user_requests_stop_near_destination() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
	}
	set_stop_and_restart_requested(simulator_timer, 10, 50, 160);
	do_refuel_if_needed();
}

void test_arrival_at_destination() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
	}
	do_refuel_if_needed();
}

void test_slowcar_lane_right() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
		
		set_slowcar_on_lane(simulator_timer, 8, 20, LANE_RIGHT, CAR_ID);
	}
	do_refuel_if_needed();
}

void test_semaphore() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
		
		set_semaphore(simulator_timer, 10, 30, 30);
	}
	do_refuel_if_needed();
}

void test_low_fuel() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
	}
	do_refuel_if_needed();
}

void test_low_oil() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
	}
	do_refuel_if_needed();
}

test_proximity_sensor(){
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
		set_proximity_sensor(simulator_timer, 10, 50);
	}
	do_refuel_if_needed();
}

void test_full() {
	if(drive_action_state.halt != ON_HALT) {
		set_position();
		set_fuel_and_oil();
		update_actual_lane_position();
		
		set_semaphore(simulator_timer, 10, 20, 20);
		set_slowcar_on_lane(simulator_timer, 70, 20, LANE_RIGHT, CAR_ID);
		set_proximity_sensor(simulator_timer, 180, 20);
	}
	set_stop_and_restart_requested(simulator_timer, 120, 20, 190);
	do_refuel_if_needed();
}

__task void InputSimulator(void){
	set_defaults(TEST_FULL);
	while(LIVE){
		IDLE = 0;
		SIMULATOR = 1;
		
		test_full();
		
		SIMULATOR = 0;
		simulator_timer++;
		sleep(10);
	}
	
	os_tsk_delete_self();
}
