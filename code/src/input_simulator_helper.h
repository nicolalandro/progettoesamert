void set_defaults(int oil_level, int fuel_level, long lat, long lon);
void set_position(void);
void set_fuel_and_oil(void);
void update_actual_lane_position(void);
void do_refuel_if_needed(void);
void set_stop_and_restart_requested(int simulator_timer, int start_time, int button_click_duration, int total_halt_duration);
void set_semaphore(int simulator_timer, int start_time, int red_duration, int green_duration);
void set_slowcar_on_lane(int simulator_timer, int start_time, int signal_duration, char lane_position, int car_id);
void set_proximity_sensor(int simulator_timer, int start_time, int signal_duration);
