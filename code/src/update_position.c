#include <RTL.h>             // RTX kernel functions & defines
#include "state.h"
#include "wait.h"

extern volatile int IDLE;
extern Sensors sensors;
extern PositionState position_state;
int POSITION = 0;

__task void UpdatePosition(void) {
	while(LIVE) {
		IDLE = 0;
		POSITION = 1;
		busy_wait(15);
		position_state.latitude = sensors.position.latitude;
		position_state.longitude = sensors.position.longitude;
		position_state.timestamp = sensors.position.timestamp;
		POSITION = 0;
		sleep(50);
	}
	
	os_tsk_delete_self();
}
