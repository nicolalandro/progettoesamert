#include <RTL.h>						
#include <stm32f10x.h>       // STM32F103 definitions
#include "state.h"
#include "drive.h"
#include "decide_drive_actions.h"
#include "update_position.h"
#include "input_simulator.h"
#include "update_internal_sensors.h"
#include "update_external_sensors.h"
#include "danger.h"

volatile int IDLE = 0;
char LIVE = 1;
int DANGER_EVENT_ID = 0x0001;
int SIMULATOR_EVENT_ID = 0x0002;

Sensors sensors;
DriveActionState drive_action_state;
PositionState position_state;
VehicleInternalState vehicle_internal_state;
VehicleExternalState vehicle_external_state;

OS_TID danger_task_id;
OS_TID input_simulator_task_id;

__task void TaskInit(void) {
	input_simulator_task_id = os_tsk_create(InputSimulator, 255);
	os_tsk_create(UpdateInternalSensors, 252);
	os_tsk_create(Drive, 238);
	os_tsk_create(DecideDriveActions, 235);
	os_tsk_create(UpdatePosition, 234);
	os_tsk_create(UpdateExternalSensors, 231);
	danger_task_id = os_tsk_create(Danger, MIN_PRIORITY);
	
	os_tsk_delete_self();
}

int main(void) {
	LIVE = 1;
	drive_action_state.halt = NO_ACTION;
	sensors.stop_requested = 0;
	sensors.restart_requested = 0;
	sensors.position.latitude = 0;
	sensors.position.longitude = 0;
	os_sys_init(TaskInit);
}
