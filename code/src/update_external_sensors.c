#include <RTL.h>
#include "state.h"
#include "wait.h"

extern volatile int IDLE;
extern Sensors sensors;
extern VehicleExternalState vehicle_external_state;
extern OS_TID danger_task_id;
extern int DANGER_EVENT_ID;

int UPDATE_EXTERNAL = 0;

void evaluate_danger(){
	if(sensors.proximity == DETECTED){
		os_tsk_prio(danger_task_id, MAX_PRIORITY);
		UPDATE_EXTERNAL = 0;
		os_evt_set(DANGER_EVENT_ID, danger_task_id);
	}
}

void update_external_sensors(){
	vehicle_external_state.stop_requested = vehicle_external_state.stop_requested || sensors.stop_requested;
	vehicle_external_state.restart_requested = vehicle_external_state.restart_requested || sensors.restart_requested;
	vehicle_external_state.lane_position = sensors.lane_position;
	vehicle_external_state.semaphore = sensors.semaphore;
	vehicle_external_state.slowcar_signal.lane_position = sensors.slowcar_signal.lane_position;
	vehicle_external_state.slowcar_signal.car_id = sensors.slowcar_signal.car_id;
}

__task void UpdateExternalSensors(void) {
	while(LIVE) {
		IDLE = 0;
		UPDATE_EXTERNAL = 1;
		
		busy_wait(30);
		
		evaluate_danger();
		UPDATE_EXTERNAL = 1;
		
		update_external_sensors();
		
		UPDATE_EXTERNAL = 0;
		sleep(200);
	}
	
	os_tsk_delete_self();
}
