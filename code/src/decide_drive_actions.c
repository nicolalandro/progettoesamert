#include <RTL.h>             // RTX kernel functions & defines
#include "state.h"
#include "wait.h"

extern volatile int IDLE;
int DECIDE = 0;
int MIN_FUEL_LEVEL = 200;
int MIN_OIL_LEVEL = 200;

char refuel_pending = 0;

extern DriveActionState drive_action_state;
extern VehicleExternalState vehicle_external_state;
extern VehicleInternalState vehicle_internal_state;
extern PositionState position_state;

short default_steer_actions[5] = {NO_ACTION, STEER_LEFT, STEER_RIGHT, CHANGE_LANE_LEFT, CHANGE_LANE_RIGHT};
short default_throttle_actions[3] = {NO_ACTION, ACCELERATE, BREAK};

int steer_action_index = 0;
int throttle_action_index = 0;
int x;

char last_slowcar_id = NO_ACTION;

void handle_red_semaphore() {
	drive_action_state.steer = NO_ACTION;
	drive_action_state.throttle = BREAK;
}

void handle_green_semaphore() {
	drive_action_state.throttle = ACCELERATE;
	drive_action_state.steer = NO_ACTION;
}

void follow_navigation() {
	steer_action_index = (steer_action_index + 1) % 5;
	throttle_action_index = (throttle_action_index + 1) % 3;
	drive_action_state.steer = default_steer_actions[steer_action_index];
	drive_action_state.throttle = default_throttle_actions[throttle_action_index];
}

char slowcar_signal_valid() {
	char valid = 0;
	//if(vehicle_external_state.slowcar_signal.car_id == last_slowcar_id || vehicle_external_state.slowcar_signal.car_id == NO_ACTION) {
	if(vehicle_external_state.slowcar_signal.car_id != NO_ACTION) {
		valid = 1;
	}
	//	valid = 0;
	//} else {
		//valid = 1;
	//}
	//last_slowcar_id = vehicle_external_state.slowcar_signal.car_id;
	return valid;
}

void change_lane() {
	drive_action_state.steer = 
		vehicle_external_state.slowcar_signal.lane_position == LANE_LEFT ?
			CHANGE_LANE_RIGHT : CHANGE_LANE_LEFT;
}

void handle_slowcar_signal() {
	if(vehicle_external_state.lane_position == vehicle_external_state.slowcar_signal.lane_position) {
		change_lane();
	} else {
		drive_action_state.steer = NO_ACTION;
	}
	drive_action_state.throttle = ACCELERATE;
}

char is_near_final_destination() {
	if(vehicle_external_state.destination_lat - position_state.latitude <= 20 && 
			vehicle_external_state.destination_lon - position_state.longitude <= 20) {
				return 1;
	} else {
		return 0;
	}
}

void set_halt_destination() {
	if(!is_near_final_destination() && drive_action_state.halt == NO_ACTION) {
		drive_action_state.halt_longitude = position_state.longitude + 10;
		drive_action_state.halt_latitude = position_state.latitude + 10;
		drive_action_state.halt = GOING_TO_HALT;		
	}
}

char arrived_to_halt() {
		if(drive_action_state.halt_latitude <= position_state.latitude 
			&& drive_action_state.halt_latitude <= position_state.longitude) {
			return 1;
		} else {
			return 0;
		}
}

char should_restart_from_halt() {
	return !refuel_pending && 
		(!vehicle_external_state.stop_requested || 
			(vehicle_external_state.stop_requested && vehicle_external_state.restart_requested));	
}

void decide() {
	if(vehicle_internal_state.fuel_level < MIN_FUEL_LEVEL || 
		vehicle_internal_state.oil_level < MIN_OIL_LEVEL ||
		vehicle_external_state.stop_requested) {
		set_halt_destination();
	}
	
	if(drive_action_state.halt == GOING_TO_HALT && arrived_to_halt()) {
			drive_action_state.halt = ON_HALT;
			refuel_pending = 1;
	}
	
	if(drive_action_state.halt == ON_HALT) {
		if(should_restart_from_halt()) {			
			vehicle_external_state.stop_requested = 0;
			vehicle_external_state.restart_requested = 0;
			drive_action_state.halt = NO_ACTION;
			drive_action_state.throttle = ACCELERATE;
		} else {
			drive_action_state.steer = NO_ACTION;
			drive_action_state.throttle = BREAK;
		}		
	}	else if(vehicle_external_state.semaphore == SEM_RED) {
		handle_red_semaphore();
	} else if (slowcar_signal_valid()) {
		handle_slowcar_signal();
	} else if(vehicle_external_state.semaphore == SEM_GREEN) {
		handle_green_semaphore();
	} else {
		follow_navigation();
	}
}

__task DecideDriveActions(void) {
	while(LIVE) {
		IDLE = 0;
		DECIDE = 1;
		
		if(vehicle_external_state.destination_lat <= position_state.latitude 
			&& vehicle_external_state.destination_lon <= position_state.longitude) {
				LIVE = 0;
				DECIDE = 0;
				break;
		}
		busy_wait (1);
		
		decide();
		
		DECIDE = 0;
		sleep(30);
	}
	
	os_tsk_delete_self();
}
