#include <RTL.h>
#include "state.h"
#include "wait.h"
#include "update_external_sensors.h"
#include "drive.h"

extern volatile int IDLE;
extern int DANGER_EVENT_ID;
extern int SIMULATOR_EVENT_ID;
extern OS_TID input_simulator_task_id;
extern DriveActionState actual_drive_actions; //for debug, read by InputSimulator

int DANGER = 0;

extern Sensors sensors;

void handle_danger() {
	do_not_throttle();
	do_change_lane_left();
	if(sensors.proximity == DETECTED) {
		do_break();
		do_not_steer();
		while(sensors.proximity == DETECTED) { }
	} else {
		do_accelerate();
		do_not_steer();
		busy_wait(10);
		do_change_lane_right();
	}
}

__task void Danger(void){
	while(LIVE) {
		IDLE = 0;
		os_evt_wait_or(DANGER_EVENT_ID, 0xFFFF);
		DANGER = 1;
		handle_danger();
		DANGER = 0;
		os_tsk_prio_self(MIN_PRIORITY);
	}
	
	os_tsk_delete_self();
}
